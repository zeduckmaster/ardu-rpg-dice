#pragma once
#include "zarduboy.h"
#include "gfx.h"

#if defined(__linux__) || defined(__APPLE__)
#include <cstdio>
finline char* itoa(int value, char* str, int base)
{
    switch(base)
    {
    case 10: sprintf(str, "%d", value); break;
    }
    return str;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// types

// size limited to 128 elements
template<typename T, int8_t N>
struct Array
{
    using SizeType = uint8_t;

    finline constexpr SizeType size() const { return N; }
    finline constexpr SizeType max_size() const { return N; }
    finline constexpr bool empty() const { return false; }
    finline T& operator[](SizeType pos) { return _data[pos]; }
    finline constexpr T const& operator[](SizeType pos) const { return _data[pos]; }
    finline T* data() { return _data; }
    finline T const* data() const { return _data; }
    finline T& front() { return _data[0]; }
    finline T const& front() const { return _data[0]; }
    finline T& back() { return _data[N - 1]; }
    finline T const& back() const { return _data[N - 1]; }

    T _data[N];
};

constexpr auto const DiceMultMax = uint8_t{20};
constexpr Array<int8_t, 8> const DiceTypes
{
    2, 4, 6, 8, 10, 12, 20, 100
};

struct RollResult
{
    bool isFastRoll = false;
    int16_t result = 0;
    int8_t dices[DiceMultMax];

    void clear()
    {
        isFastRoll = false;
        result = 0;
        memset(dices, 0, sizeof(int8_t) * DiceMultMax);
    }
};

struct Formula
{
    int8_t diceMult = 0;
    int8_t diceType = 0;
    int8_t modSign = 0;
    int8_t modValue = 0;

    finline int8_t diceTypeValue() const
    {
        return DiceTypes[diceType];
    }

    void incrDiceMult(int8_t value)
    {
        diceMult += value;
        if(diceMult < 1)
            diceMult = DiceMultMax;
        else if(diceMult > DiceMultMax)
            diceMult = 1;
    }

    void incrDiceType(int8_t value)
    {
        diceType += value;
        if(diceType < 0)
            diceType = DiceTypes.size() - 1;
        else if(diceType >= DiceTypes.size())
            diceType = 0;
    }

    void incrModSign(int8_t value)
    {
        unused(value);
        modSign *= -1;
    }

    void incrModValue(int8_t value)
    {
        modValue += value;
        if(modValue < 0)
            modValue = 20;
        else if(modValue > 20)
            modValue = 0;
    }

    void roll(RollResult& rr)
    {
        rr.clear();
        auto diceTotal = 0;
        for(auto n = 0; n < diceMult; ++n)
        {
            auto rd = random(1, diceTypeValue() + 1);
            rr.dices[n] = rd;
            diceTotal += rd;
        }
        rr.result = diceTotal + (modSign * modValue);
    }

    finline void fastRoll(RollResult& rr)
    {
        roll(rr);
        rr.isFastRoll = true;
    }

    bool operator==(Formula const& f) const
    {
        return (diceMult == f.diceMult)
            && (diceType == f.diceType)
            && (modSign == f.modSign)
            && (modValue == f.modValue);
    }

    finline bool operator!=(Formula const& f) const
    {
        return !(*this == f);
    }
};

struct PrevFormulas
{
    static int8_t const MaxSize = 5;

    finline Formula const& last() const { return _data[_last]; }
    finline void readReset() { _read = _last; }
    void readNext(Formula& f)
    {
        if(f == _data[_read])
            _read = (_read > 0)? _read - 1 : MaxSize - 1;
        if(_data[_read].diceMult == 0)
            readReset();
        if(_data[_read].diceMult == 0)
            return;
        f = _data[_read];
        _read = (_read > 0)? _read - 1 : MaxSize - 1;
    }

    void push(Formula const& value)
    {
        _last = (_last + 1) % MaxSize;
        _data[_last] = value;
    }

    Formula _data[MaxSize];
    int8_t _last = 0;
    int8_t _read = 0;
};

////////////////////////////////////////////////////////////////////////////////
// globals

ZArduboy Z;

constexpr auto const FrameTime  = int32_t{1000/30}; // 30fps
auto lasttime = uint32_t{0};
auto frame = uint32_t{0};
auto previousButtonState = uint8_t{0};

constexpr auto const DiceMultPosX = uint8_t{0};
constexpr auto const DiceTypePosX = uint8_t{DiceMultPosX + (font8_Width + 1) * 2};
constexpr auto const ModSignPosX = uint8_t{DiceTypePosX + (font8_Width + 1) * 4};
constexpr auto const ModValuePosX = uint8_t{ModSignPosX + (font8_Width + 1)};
constexpr auto const EqualPosX = uint8_t{ModValuePosX + (font8_Width + 1) * 2};

auto cursorPos = int8_t{0};
auto cursorAnim = int8_t{0};
auto formula = Formula{};
auto prevFormulas = PrevFormulas{};
RollResult rollResult;

////////////////////////////////////////////////////////////////////////////////
// game funcs

finline bool buttonPressed(uint8_t button, uint8_t buttonState)
{
    return (!(button & buttonState)) && (button & previousButtonState);
}

finline bool buttonFirstPress(uint8_t button, uint8_t buttonState)
{
    return (button & buttonState) && (!(button & previousButtonState));
}

finline bool handleButtonAnyPress()
{
    auto buttonState = Z.buttonState();
    auto r = (buttonState != 0) && (previousButtonState == 0);
    previousButtonState = buttonState;
    return r;
}

finline void drawBitmapEx(int16_t x, int16_t y, uint8_t const* bitmap, uint8_t w, uint8_t h, bool invert)
{
    Z.drawBitmapEx(x, y, bitmap, w, h, false, invert);
}

finline void drawBitmap(int16_t x, int16_t y, uint8_t const* bitmap, uint8_t w, uint8_t h)
{
    Z.drawBitmapEx(x, y, bitmap, w, h, false, false);
}

void drawChar(int16_t x, int16_t y, char value)
{
    if((value >= 48) && (value <= 57))
    {
        auto digit = static_cast<uint8_t>(value - 48);
        drawBitmap(x, y, font8_Numbers + (digit * font8_Width), font8_Width, font8_Height);
    }
    else
    {
        auto exCharPos = int8_t{-1};
        switch(value)
        {
        case '-': exCharPos = font8_Pos_neg; break;
        case '+': exCharPos = font8_Pos_pos; break;
        case '=': exCharPos = font8_Pos_equal; break;
        case '(': exCharPos = font8_Pos_lparen; break;
        case ')': exCharPos = font8_Pos_rparen; break;
        case ':': exCharPos = font8_Pos_colon; break;
        case ',': exCharPos = font8_Pos_comma; break;
        case ' ':
        default:
            return;
        }
        drawBitmap(x, y, font8_Extras + (exCharPos * font8_Width), font8_Width, font8_Height);
    }
}

void drawString(int16_t x, int16_t y, char* str, uint8_t count)
{
    for(auto n = uint8_t{0}; n < count; ++n)
        drawChar(x + n * (font8_Width + 1), y, str[n]);
}

void drawNumbers(int16_t x, int16_t y, int16_t value)
{
    char buf[6];
    itoa(value, buf, 10);
    auto len = strlen(buf);
    drawString(x, y, buf, len);
}

void drawResult(int16_t x, int16_t y, int16_t value)
{
    char buf[6];
    itoa(value, buf, 10);
    auto len = static_cast<uint8_t>(strlen(buf));
    x -= (len * font14_fantasy_Width + 1) / 2;
    auto nstart = uint8_t{0};
    if(value < 0)
    {
        nstart = 1;
        drawBitmap(x, y, font14_fantasy_numbers + 10 * font14_fantasy_CharSize, font14_fantasy_Width, font14_fantasy_Height);
    }
    for(auto n = nstart; n < len; ++n)
    {
        auto c = buf[n];
        if((c >= 48) && (c <= 57))
        {
            auto digit = static_cast<uint8_t>(c - 48);
            drawBitmap(x + n * (font14_fantasy_Width + 1), y, font14_fantasy_numbers + (digit * font14_fantasy_CharSize), font14_fantasy_Width, font14_fantasy_Height);
        }
    }
}

void drawFormula(int16_t x, int16_t y, Formula const& f)
{
    if(f.diceMult == 0)
        return;
    drawNumbers(x + DiceMultPosX, y, f.diceMult);
    drawNumbers(x + DiceTypePosX + font8_Width + 1, y, f.diceTypeValue());
    drawChar(x + ModSignPosX, y, (f.modSign >= 0)? '+' : '-');
    drawNumbers(x + ModValuePosX, y, f.modValue);
}

void drawCursor(int16_t fx, int16_t fy)
{
    auto y0 = fy - 2;
    auto y1 = y0 + font8_Height + 3;
    auto sx = fx - 1;
    auto x0 = 0;
    auto x1 = 0;
    switch(cursorPos)
    {
    case 0:
        x0 = sx;
        x1 = sx + DiceTypePosX;
        break;
    case 1:
        x0 = sx + DiceTypePosX;
        x1 = sx + ModSignPosX;
        break;
    case 2:
        x0 = sx + ModSignPosX;
        x1 = sx + ModValuePosX;
        break;
    case 3:
        x0 = sx + ModValuePosX;
        x1 = x0 + (font8_Width + 1) * 2;
        break;
    }
    auto xmid = x0 + (x1 - x0) / 2 - font8_Width / 2;
    if(cursorAnim != 0)
        cursorAnim += (cursorAnim > 0)? -1: 1;
    if(cursorAnim <= 0)
        drawBitmap(xmid, y0 - 4, font8_Extras + font8_Pos_up * font8_Width, font8_Width, font8_Height);
    if(cursorAnim >= 0)
        drawBitmap(xmid, y1 - 1, font8_Extras + font8_Pos_down * font8_Width, font8_Width, font8_Height);
    Z.drawHLine(x0, y1, x1, eColor::White);
}

void drawRollDiceResults(int16_t y, RollResult const& rr)
{
    auto cx = 0;
    auto cy = 0;
    for(auto n = uint8_t{0}; n < DiceMultMax; ++n)
    {
        auto dr = rr.dices[n];
        if(dr == 0)
            break;
        char buf[6];
        itoa(dr, buf, 10);
        auto len = strlen(buf);
        if((cx + len) >= 21)
        {
            ++cy;
            cx = 0;
        }
        auto wx = cx * (font8_Width + 1);
        auto wy = cy * (font8_Height + 1) + y;
        drawString(wx, wy, buf, len);
        cx += len;
        wx = cx * (font8_Width + 1);
        drawChar(wx, wy, ',');
        ++cx;
    }
}

void gameFunc()
{
    // input
    auto incr = 0;
    {
        auto buttonState = Z.buttonState();
        if(buttonFirstPress(Z.InputRight, buttonState) == true)
        {
            ++cursorPos;
            if(cursorPos > 3)
                cursorPos = 0;
        }
        else if(buttonFirstPress(Z.InputLeft, buttonState) == true)
        {
            --cursorPos;
            if(cursorPos < 0)
                cursorPos = 3;
        }
        else if(buttonFirstPress(Z.InputUp, buttonState) == true)
        {
            incr = 1;
            cursorAnim = 5;
        }
        else if(buttonFirstPress(Z.InputDown, buttonState) ==true)
        {
            incr = -1;
            cursorAnim = -5;
        }
        else if(buttonPressed(Z.InputA, buttonState) == true)
        {
            formula.roll(rollResult);
            if(prevFormulas.last() != formula)
                prevFormulas.push(formula);
            prevFormulas.readReset();
        }
        else if(buttonState & Z.InputA)
        {
            formula.fastRoll(rollResult);
        }
        else if(buttonPressed(Z.InputB, buttonState) == true)
        {
            prevFormulas.readNext(formula);
        }
        previousButtonState = buttonState;
    }

    // update formula
    if(incr != 0)
    {
        prevFormulas.readReset();
        switch(cursorPos)
        {
        case 0: formula.incrDiceMult(incr); break;
        case 1: formula.incrDiceType(incr); break;
        case 2: formula.incrModSign(incr); break;
        case 3: formula.incrModValue(incr); break;
        }
    }

    // draw result
    drawResult(64, 0, rollResult.result);
    Z.drawHLine(32, 15, Z.LCDWidth - 33, eColor::White);
    if(rollResult.isFastRoll == false)
        drawRollDiceResults(17, rollResult);
    // draw bottom
    drawBitmap(0,42, bmpFrameFormula, bmpFrameFormula_Width, bmpFrameFormula_Height);
    drawBitmapEx(60, 42, bmpButtonRoll, bmpButtonRoll_Width, bmpButtonRoll_Height, previousButtonState & Z.InputA);
    drawBitmapEx(99, 42, bmpButtonBack, bmpButtonBack_Width, bmpButtonBack_Height, previousButtonState & Z.InputB);
    // draw cursor and dice formula
    auto yF = Z.LCDHeight - 15;
    drawCursor(3, yF);
    drawFormula(3, yF, formula);
}

////////////////////////////////////////////////////////////////////////////////
// main program

void zsetup()
{
    // boot
    Z.begin();
    Z.initRandomSeed();
    // init formula
    formula.diceMult = 1;
    formula.diceType = 2;
    formula.modSign = 1;
    formula.modValue = 0;
}

void zloop()
{
    // control frame time
    {
        auto curtime = millis();
        auto elapsedTime = curtime - lasttime;
        if(elapsedTime < FrameTime)
        {
            if((FrameTime - elapsedTime) > 1)
                Z.idle();
            return;
        }
        lasttime = curtime;
        ++frame;
    }
    gameFunc();
    Z.display();
}



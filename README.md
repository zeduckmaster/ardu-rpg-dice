A small and fast application for Arduboy to roll dice.

![ardu-rpg-dice](https://gitlab.com/zeduckmaster/ardu-rpg-dice/raw/master/screen0.png "ardu-rpg-dice screenshot")

# How to use

* Use D-Pad (arrows on desktop) to move the cursor and change the dice roll expression different values.
* Use A to roll (hold A to shake the dice and the odds).
* Use B to recall previous dice roll expression (up to the last 5 are remembered).

On desktop, you can quit using *Escape* key.

# Binaries

* arduboy: [ardu-rpg-dice-1.0.hex](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-rpg-dice)
* arduboy: [ardu-rpg-dice-1.0.arduboy](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-rpg-dice)
* windows 64bits: [ardu-rpg-dice-1.0.7z](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-rpg-dice)
* linux 64bits (Ubuntu 18.04): [ardu-rpg-dice-1.0-x86_64.AppImage](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-rpg-dice)

# How to build

## Arduboy

The game has been compiled with the official Arduino ide 1.8.9 and tested successfuly on Arduboy v1.

## Desktop

The game can be compiled for windows, linux and macOS with the provided Qt Creator project.

Building for macOS and linux require to install SDL2 framework.

# Information

License: GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html
